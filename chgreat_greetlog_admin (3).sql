-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 02, 2019 at 10:26 AM
-- Server version: 5.7.28
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chgreat_greetlog_admin`
--

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE `branches` (
  `branch_id` int(11) NOT NULL,
  `companytype` varchar(225) NOT NULL,
  `companyid` int(225) NOT NULL,
  `branchname` varchar(255) NOT NULL,
  `logo` varchar(225) NOT NULL,
  `slogan` text NOT NULL,
  `location` varchar(225) NOT NULL,
  `branchhead` varchar(225) NOT NULL,
  `email_id` varchar(225) NOT NULL,
  `country` varchar(100) NOT NULL,
  `contactno` varchar(225) NOT NULL,
  `alternateno` varchar(225) NOT NULL,
  `plan` varchar(225) NOT NULL,
  `is_expired` varchar(225) NOT NULL,
  `renewdate` varchar(225) NOT NULL,
  `expirydate` varchar(225) NOT NULL,
  `sendtype` varchar(225) NOT NULL,
  `access` varchar(225) NOT NULL,
  `bpassword` varchar(225) NOT NULL,
  `createdate` date NOT NULL,
  `modifieddate` date NOT NULL,
  `is_delete` int(225) NOT NULL,
  `token` varchar(225) NOT NULL,
  `divaice_id` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branches`
--

INSERT INTO `branches` (`branch_id`, `companytype`, `companyid`, `branchname`, `logo`, `slogan`, `location`, `branchhead`, `email_id`, `country`, `contactno`, `alternateno`, `plan`, `is_expired`, `renewdate`, `expirydate`, `sendtype`, `access`, `bpassword`, `createdate`, `modifieddate`, `is_delete`, `token`, `divaice_id`) VALUES
(1, 'Corporation', 1, 'branchone', 'http://greetlogs.com/greetlog_admin/uploads/login_bg5.png', 'no slogen', 'varlin', 'Justin bibar', 'Justin@gmail.com', 'VI', '+1 (556) 646-5446', '', '15 days', '', '1571682600', '1572978600', 'Email', 'full', '', '2019-10-22', '0000-00-00', 0, '', ''),
(2, 'College/University', 1, 'sayfdsfsd', 'http://greetlogs.com/greetlog_admin/uploads/down12.jpg', 'sdas', 'ifoeer', '1dssdds', 'nmdff', 'IN', '+91 3242342342', '', '15', '2019-11-07', '1571682600', '1572978600', 'Email', 'view', '', '2019-10-22', '0000-00-00', 0, '', ''),
(3, 'Company', 16, 'IBM,Bhawar kua', 'http://greetlogs.com/greetlog_admin/uploads/719.jpg', '', 'indore', 'Dany Danjongpa', 'badalviroliya@gmail.com', 'IN', '+91 3445656777', '', '15 days', '', '1572460200', '1573756200', 'Email', 'view', 'MTIzNDU2Nzg5MA==', '2019-10-31', '0000-00-00', 0, '', ''),
(6, 'Company', 39, 'branchone', 'http://greetlogs.com/greetlog_admin/uploads/login_bg7.png', 'gg', 'harsud', 'naveen', 'naveen.rajput@acesskynet.com', 'IN', '+91 9981787252', '', '15 days', '', '1572546600', '1573842600', 'Email', 'view', 'MTIzNDU2Nzg5MA==', '2019-11-01', '0000-00-00', 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `cakedesertsession_management`
--

CREATE TABLE `cakedesertsession_management` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `framework_token` varchar(255) NOT NULL,
  `created_on` datetime NOT NULL,
  `browser` varchar(255) DEFAULT NULL,
  `operating_system` varchar(255) DEFAULT NULL,
  `expired_to` datetime NOT NULL,
  `updated_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cakedesertsession_management`
--

INSERT INTO `cakedesertsession_management` (`id`, `userid`, `token`, `ip`, `framework_token`, `created_on`, `browser`, `operating_system`, `expired_to`, `updated_on`) VALUES
(0, 0, 'fe8a8fb390487494a055762e59a3024f', '::1', '1519839053', '2018-02-28 06:33:23', 'Chrome', 'Windows 7', '2018-03-01 18:33:23', '2018-02-28 06:33:23'),
(0, 2, '81e29ddba47895e14db8a0fdc3a2b213', '::1', '1519839923', '2018-02-28 06:47:17', 'Chrome', 'Windows 7', '2018-03-01 20:15:01', '2018-02-28 20:15:01'),
(0, 3, '9b28e15f16fd83bb2faf115b4494b222', '::1', '1519845171', '2018-02-28 08:15:20', 'Chrome', 'Windows 7', '2018-03-01 20:33:53', '2018-02-28 20:33:53'),
(0, 0, 'fe8a8fb390487494a055762e59a3024f', '::1', '1519839053', '2018-02-28 06:33:23', 'Chrome', 'Windows 7', '2018-03-01 18:33:23', '2018-02-28 06:33:23'),
(0, 2, '81e29ddba47895e14db8a0fdc3a2b213', '::1', '1519839923', '2018-02-28 06:47:17', 'Chrome', 'Windows 7', '2018-03-01 20:15:01', '2018-02-28 20:15:01'),
(0, 3, '9b28e15f16fd83bb2faf115b4494b222', '::1', '1519845171', '2018-02-28 08:15:20', 'Chrome', 'Windows 7', '2018-03-01 20:33:53', '2018-02-28 20:33:53');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` int(11) NOT NULL,
  `companyname` varchar(255) NOT NULL,
  `companytype` varchar(100) NOT NULL,
  `logo` varchar(355) NOT NULL,
  `slogan` text NOT NULL,
  `prefix` varchar(100) DEFAULT NULL,
  `ownername` varchar(255) NOT NULL,
  `sendtype` varchar(100) NOT NULL,
  `country` varchar(100) NOT NULL,
  `contactno` varchar(15) NOT NULL,
  `alternateno` varchar(15) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(250) NOT NULL,
  `address` text NOT NULL,
  `access` varchar(20) NOT NULL DEFAULT 'full',
  `plan` varchar(100) NOT NULL,
  `is_expired` varchar(100) NOT NULL,
  `renewdate` varchar(10) NOT NULL,
  `expirydate` varchar(10) NOT NULL,
  `createdate` date NOT NULL,
  `modifieddate` date NOT NULL,
  `is_delete` int(11) NOT NULL,
  `token` varchar(225) NOT NULL,
  `divaice_id` varchar(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `companyname`, `companytype`, `logo`, `slogan`, `prefix`, `ownername`, `sendtype`, `country`, `contactno`, `alternateno`, `email`, `password`, `address`, `access`, `plan`, `is_expired`, `renewdate`, `expirydate`, `createdate`, `modifieddate`, `is_delete`, `token`, `divaice_id`) VALUES
(7, 'abc', 'Company Name', '', '', 'Miss', 'kavita', '', 'IN', '+91 9993507196', '', 'kavita.singh@acesskynet.com', '40a1f2be8164c525aaf740bb5e1ef0a4', '', 'full', '', '', '', '', '2019-10-23', '0000-00-00', 0, '', NULL),
(8, 'Axe\'s Pvt Ltd', 'Company Name', '', '', 'Mr', 'Axes', '', 'IN', '+91 7897897897', '', 'Axes@mailinator.com', '1e1873dd68d9db457cca4ca3edf8b939', '', 'full', '15', '', '2019-10-23', '', '2019-10-23', '0000-00-00', 0, '', NULL),
(13, 'Tsc', 'Company Name', '', '', '', 'amit badhana', '', 'IN', '+91 6878678978', '', 'divbhawsar@gmail.com', 'e807f1fcf82d132f9bb018ca6738a19f', '', 'full', '', '', '', '', '2019-10-24', '0000-00-00', 0, '', NULL),
(15, 'abc', 'Company Name', '', '', NULL, 'kv qw', '', 'IN', '+91 2345678901', '', 'kavitasingh.2190@gmail.com', '', '', 'full', '', '', '', '', '2019-10-24', '0000-00-00', 0, '', NULL),
(16, 'IBM', 'Company Name', 'http://greetlogs.com/greetlog_admin/uploads/downlogo.jpg', '', 'Mr', 'Rakesh khurana', 'Email', 'IN', '+91 5678898097', '', 'badalmiroliya19@gmail.com', 'MTIzNDU2Nzg5MA==', 'palasiya,indore', 'full', '15 days', '', '1572633000', '1573929000', '2019-10-31', '0000-00-00', 0, '', NULL),
(17, 'Ritz cars', 'Ritz cars', '', '', NULL, 'Ritz crackers', '', '', '8656886683', '', 'Ritz@mailinator.com', 'baf8fead0c908280cc0b1c4cdee57d4a', '', 'full', '', '', '', '', '2019-11-01', '0000-00-00', 0, '7894654151565', NULL),
(18, 'ved cars', 'ved cars', '', '', NULL, 'Ritz crackers', '', '', '8656886683', '', 'ved@mailinator.com', '8f4945af169212a06dbb2f974110221d', '', 'full', '', '', '', '', '2019-11-01', '0000-00-00', 0, '7894654151565', NULL),
(19, 'aces', 'new', '', '', NULL, 'mr. chanchal', '', '', '4871787252', '', 'admdin@gmail.com', '9c3e4854858f1685d5747d1671e174f2', '', 'full', '', '', '', '', '2019-11-01', '0000-00-00', 0, '1519839053', NULL),
(21, 'zyx', 'Corporation', '', '', NULL, 'kavita singh', '', 'IN', '+91 9993507196', '', 'kavita.acesskynet@gmail.com', 'e807f1fcf82d132f9bb018ca6738a19f', '', 'full', '', '', '', '', '2019-11-01', '0000-00-00', 0, '', NULL),
(23, 'Getaway price', 'Getaway price', '', '', NULL, 'Getaway', '', '', '8956898596', '', 'geta@mailinator.com', '68043677c87ea20659be3402d073b393', '', 'full', '', '', '', '', '2019-11-01', '0000-00-00', 0, '7894654151565', NULL),
(24, 'Metadata pvt', 'Metadata pvt', '', '', NULL, 'Meta', '', '', '7559547578', '', 'meta@mailinator.com', '3aec53491fdff1a58ddbe0ece17f378a', '', 'full', '', '', '', '', '2019-11-01', '0000-00-00', 0, '7894654151565', NULL),
(25, 'bchccjccnjc', 'bchccjccnjc', '', '', NULL, 'Hxdgfk', '', '', '7585528577', '', 'fucuuff@maiinator.com', '7993cd102846b49afa039b6caa631d11', '', 'full', '', '', '', '', '2019-11-01', '0000-00-00', 0, '7894654151565', NULL),
(27, 'Sandman', 'Sandman', '', '', NULL, 'SJ', '', '', '7568568965', '', 'Sandman@mailinator.com', 'a82890d72040e0acb3b036eaf1529d9b', '', 'full', '', '', '', '', '2019-11-01', '0000-00-00', 0, '7894654151565', NULL),
(39, 'aces', 'Company', '', '', NULL, 'naveen singh rajput', '', 'IN', '+91 9981787252', '', 'naveen.rajput@acesskynet.com', '071192c8d403cbf6f58930672d1abfd0', '', 'full', '', '', '', '', '2019-11-01', '0000-00-00', 0, '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `emp_id` int(11) NOT NULL,
  `prefix` varchar(100) DEFAULT NULL,
  `empname` varchar(100) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `access` varchar(100) NOT NULL,
  `profile_pic` varchar(300) NOT NULL,
  `country` varchar(100) NOT NULL,
  `contact_num` varchar(15) NOT NULL,
  `alternate_num` varchar(15) NOT NULL,
  `email_id` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `dob` varchar(100) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `branchid` int(11) NOT NULL,
  `companyid` int(11) NOT NULL,
  `createdate` date NOT NULL,
  `modifieddate` date DEFAULT NULL,
  `is_delete` int(11) NOT NULL,
  `token` varchar(225) DEFAULT NULL,
  `divaice_id` varchar(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`emp_id`, `prefix`, `empname`, `gender`, `access`, `profile_pic`, `country`, `contact_num`, `alternate_num`, `email_id`, `password`, `dob`, `designation`, `branchid`, `companyid`, `createdate`, `modifieddate`, `is_delete`, `token`, `divaice_id`) VALUES
(3, '', 'mr. chanchal', '', '', '', 'country', '6260490558', '', 'naveen.rajpu@acesskynet.com', '61857e5929ba70e7f14542a6bd367545', '', '', 0, 5, '2019-10-23', NULL, 0, '1519839053', NULL),
(4, '', 'mr. chanchal', '', '', '', 'country', '6260490558', '', 'naveen.rajput@acesskynet.com', '971d2604f93815300d88e71e56208cb0', '', '', 0, 6, '2019-10-23', NULL, 0, '1519839053', NULL),
(5, '', 'badal miroliya', 'Male', '', 'http://greetlogs.com/greetlog_admin/uploads/717.jpg', '', '+91 5675876897', '', 'badaal@gmail.com', 'f2fbff28d401cb19fe3cd475f9fc81a0', '2019-10-08', 'Employee', 0, 1, '2019-10-23', NULL, 0, NULL, NULL),
(6, 'Mr', 'Rakesh  khandelwal', 'Male', '', '', '', '+91 5647789887', '', 'badalmirolia19@gmail.com', '5b2ef54bf7af067a9829dcd446acd1bc', '', 'Emp', 0, 6, '2019-10-23', NULL, 0, NULL, NULL),
(7, 'Mr', 'Bushan  Bharat', 'Male', '', 'http://greetlogs.com/greetlog_admin/uploads/718.jpg', '', '+91 5667868678', '', 'rk@gmail.com', '2f9f0c3882cd8dac8873e3ae50c3ab43', '', 'emp', 0, 6, '2019-10-23', NULL, 0, NULL, NULL),
(8, 'Miss', '56565767 75767 676654', 'Male', '', 'http://greetlogs.com/greetlog_admin/uploads/down13.jpg', '', '+91 ', '', 'Agf@gmail.com', 'b83d35dd19c3ebbadb096a94ce52fa9d', '', '45676', 0, 6, '2019-10-23', NULL, 0, NULL, NULL),
(9, '', 'anil', '', '', '', '', '+91 5678789767', '', 'divbhawsar@gmail.com', '', '', '', 0, 9, '0000-00-00', NULL, 0, NULL, NULL),
(10, '', 'Anil', '', '', '', '', '+91 5675867879', '', 'divbhawsar@gmail.com', '', '', '', 0, 11, '2019-10-24', NULL, 0, NULL, NULL),
(11, '', 'amit badhana', 'Male', '', '', '', '+91 6878678978', '', 'divbhawsar@gmail.com', '', '', 'Admin', 0, 13, '2019-10-24', '2019-10-24', 0, NULL, NULL),
(12, NULL, 'Aniket Kumar Yadav', 'Male', '', 'http://greetlogs.com/greetlog_admin/uploads/img54.jpg', '', '+91 5678898090', '', 'badalviroliya@gmail.com', '2197507451682f10731deda9e62bf1ed', '', 'Employee', 0, 13, '2019-10-24', '2019-10-24', 0, NULL, NULL),
(13, NULL, 'kv sg', '', '', '', '', '+91 6789012345', '', 'kavitasingh.2190@gmail.com', '', '', '', 0, 14, '2019-10-24', NULL, 0, NULL, NULL),
(14, NULL, 'kv qw', '', '', '', '', '+91 2345678901', '', 'kavitasingh.2190@gmail.com', '', '', '', 0, 15, '2019-10-24', NULL, 0, NULL, NULL),
(15, NULL, 'Rakesh khurana', 'Male', '', 'http://greetlogs.com/greetlog_admin/uploads/download15.jpg', '', '+91 5678898097', '', 'badalmiroliya19@gmail.com', '', '', 'Admin', 0, 16, '2019-10-31', '2019-10-31', 0, NULL, NULL),
(16, 'Mr', 'Ram Shrivastva', 'Male', '', '', '', '+91 5667878978', '', 'ram@gmail.com', 'cfa303332df2445c4838b0542e4e58d3', '', 'Employee', 0, 16, '2019-10-31', NULL, 0, NULL, NULL),
(17, 'Mr', 'rakesh Shrivastva', 'Male', '', '', '', '+91 5667685855', '', 'Rakesh@gmail.com', 'a9a0047d56408b9d9fb68ccf0ca213b7', '', 'Employee', 0, 16, '2019-10-31', NULL, 0, NULL, NULL),
(18, 'Mr', 'ranjan kumar Shrivastva', 'Male', '', '', '', '+91 578899', '', 'ranjan@gmail.com', 'e8ca242f205f70507cba8b9ac9c136ca', '', 'Employee', 0, 16, '2019-10-31', NULL, 0, NULL, NULL),
(19, 'Mr', 'Raj kumar Yadav', 'Male', '', 'http://greetlogs.com/greetlog_admin/uploads/720.jpg', '', '+91 6878989978', '', 'raj@gmail.com', '1a3b7c354274dc12da4884b942d65c2b', '2018-12-31', '', 3, 16, '2019-10-31', '2019-10-31', 0, NULL, NULL),
(20, 'Mr', 'Randeep singh hudda', 'Male', 'full', 'http://greetlogs.com/greetlog_admin/uploads/img55.jpg', '', '+91 8898987675', '', 'Randeep@gmail.com', 'a4f44ccf59ab2f5b191b93922b70f218', '', 'Employee', 3, 16, '2019-10-31', '2019-10-31', 0, NULL, NULL),
(21, NULL, 'mr. chanchal', '', '', '', 'country', '6260490558', '', 'naveen.rajputi@acesskynet.com', '4b6cf6df14faaa25fc70d4c2a0bb852b', '', '', 0, 20, '2019-11-01', NULL, 0, '1519839053', NULL),
(22, NULL, 'kavita singh', '', '', '', '', '+91 9993507196', '', 'kavita.acesskynet@gmail.com', '', '', '', 0, 21, '2019-11-01', NULL, 0, NULL, NULL),
(23, NULL, 'mr. chanchal', '', '', '', 'country', '6260490558', '', 'naveen.rajsdputi@acesskynet.com', '5af79f255d51856865991723b7004d9d', '', '', 0, 22, '2019-11-01', NULL, 0, '1519839053', NULL),
(24, NULL, 'Getaway', '', '', '', 'India', '8956898596', '', 'geta@mailinator.com', '68043677c87ea20659be3402d073b393', '', '', 0, 23, '2019-11-01', NULL, 0, '7894654151565', NULL),
(25, NULL, 'Meta', '', '', '', 'India', '7559547578', '', 'meta@mailinator.com', '3aec53491fdff1a58ddbe0ece17f378a', '', '', 0, 24, '2019-11-01', NULL, 0, '7894654151565', NULL),
(26, NULL, 'Hxdgfk', '', '', '', 'India', '7585528577', '', 'fucuuff@maiinator.com', '7993cd102846b49afa039b6caa631d11', '', '', 0, 25, '2019-11-01', NULL, 0, '7894654151565', NULL),
(27, NULL, 'naveen singh rajput', '', '', '', '', '+91 9981787252', '', 'naveen.rajput@acesskynet.com', '', '', '', 0, 26, '2019-11-01', NULL, 0, NULL, NULL),
(28, NULL, 'SJ', '', '', '', 'India', '7568568965', '', 'Sandman@mailinator.com', 'a82890d72040e0acb3b036eaf1529d9b', '', '', 0, 27, '2019-11-01', NULL, 0, '7894654151565', NULL),
(29, NULL, 'navreen singh rqajput', '', '', '', '', '+91 9981787252', '', 'naveen.rajput@acesskynet.com', '', '', '', 0, 28, '2019-11-01', NULL, 0, NULL, NULL),
(30, NULL, 'naveen singh rajput', '', '', '', '', '+91 9981787252', '', 'naveen.rajput@acesskynet.com', '', '', '', 0, 29, '2019-11-01', NULL, 0, NULL, NULL),
(31, NULL, 'naveen singh rajput', '', '', '', '', '+91 9981787252', '', 'naveen.rajput@acesskynet.com', '', '', '', 0, 30, '2019-11-01', NULL, 0, NULL, NULL),
(32, NULL, 'naveen singh rajput', '', '', '', '', '+91 9981787252', '', 'naveen.rajput@acesskynet.com', '', '', '', 0, 31, '2019-11-01', NULL, 0, NULL, NULL),
(33, NULL, 'naveen singh rajput', '', '', '', '', '+91 9981787252', '', 'naveen.rajput@acesskynet.com', '', '', '', 0, 32, '2019-11-01', NULL, 0, NULL, NULL),
(34, NULL, 'NAVEEN singh rajput', '', '', '', '', '+91 9981787252', '', 'nkrajput.ec@gmail.com', '', '', '', 0, 33, '2019-11-01', NULL, 0, NULL, NULL),
(35, NULL, 'NAVEEN singh rajput', '', '', '', '', '+91 9981787252', '', 'nkrajput.ec@gmail.com', '', '', '', 0, 34, '2019-11-01', NULL, 0, NULL, NULL),
(36, NULL, 'naveen singh rajput', '', '', '', '', '+91 9981787252', '', 'nkrajput.ec@gmail.com', '', '', '', 0, 35, '2019-11-01', NULL, 0, NULL, NULL),
(37, NULL, 'naveen singh rajput', '', '', '', '', '+91 9981787252', '', 'naveen.rajput@ACESSKYNET.COM', '', '', '', 0, 36, '2019-11-01', NULL, 0, NULL, NULL),
(38, NULL, 'naveen singh rajput', '', '', '', '', '+91 9981787252', '', 'naveen.rajput@acesskynet.com', '', '', '', 0, 37, '2019-11-01', NULL, 0, NULL, NULL),
(39, NULL, 'naveen singh rajput', '', '', '', '', '+91 9981787252', '', 'naveen.rajput@acesskynet.com', '', '', '', 0, 38, '2019-11-01', NULL, 0, NULL, NULL),
(40, NULL, 'naveen singh rajput', '', '', '', '', '+91 9981787252', '', 'naveen.rajput@acesskynet.com', '', '', '', 0, 39, '2019-11-01', NULL, 0, NULL, NULL),
(41, NULL, 'Ajit Singh', '', '', '', '', '+91 4567676577', '', 'badalviroliya@gmail.com', '', '', '', 0, 40, '2019-11-01', NULL, 0, NULL, NULL),
(42, NULL, 'Raj Shekhar Raja', '', '', '', '', '+91 4667687898', '', 'badalviroliya@gmail.com', '', '', '', 0, 41, '2019-11-02', NULL, 0, NULL, NULL),
(43, NULL, 'Raj Hans', 'Male', '', '', '', '+91 5678678567', '', 'badalddff@gmail.com', 'QGdCUWpMbUs=', '', 'Employee', 0, 16, '2019-11-02', NULL, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `invites`
--

CREATE TABLE `invites` (
  `invite_id` int(11) NOT NULL,
  `companyid` int(11) NOT NULL,
  `branchid` int(11) NOT NULL,
  `branch` varchar(355) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `host` varchar(355) NOT NULL,
  `visitortype` varchar(355) NOT NULL,
  `visitor_name` varchar(355) NOT NULL,
  `contactnum` varchar(355) NOT NULL,
  `email` varchar(355) NOT NULL,
  `sendtype` varchar(355) NOT NULL,
  `title` text NOT NULL,
  `msg` text NOT NULL,
  `invitedate` varchar(355) NOT NULL,
  `invitetime` varchar(355) NOT NULL,
  `createdate` date NOT NULL,
  `modifieddate` date NOT NULL,
  `is_delete` int(11) NOT NULL,
  `summary` varchar(1225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invites`
--

INSERT INTO `invites` (`invite_id`, `companyid`, `branchid`, `branch`, `emp_id`, `host`, `visitortype`, `visitor_name`, `contactnum`, `email`, `sendtype`, `title`, `msg`, `invitedate`, `invitetime`, `createdate`, `modifieddate`, `is_delete`, `summary`) VALUES
(1, 1, 2, '', 1, '', '', '2019-10-03', '6260490558', '00:00:00', 'single', '', 'naveen.rajput@gmail.com', '2019-10-03', '00:00:00', '2019-10-22', '0000-00-00', 0, 'hi tttt hair vkfrj xsdjbgy'),
(2, 2, 0, '', 1, '', '', '{', '{', '{', 'single', '', 'gg', '22/10/2019', '10:47 AM', '2019-10-22', '0000-00-00', 0, 'vv'),
(3, 2, 0, '', 1, '', '', '2019-10-03', '6260490558', '00:00:00', 'single', '', 'naveen.rajput@gmail.com', '2019-10-03', '00:00:00', '2019-10-22', '0000-00-00', 0, 'hi tttt hair vkfrj xsdjbgy'),
(4, 2, 0, '', 1, '', '', '{', '{', '{', 'single', '', 'bx', '22/10/2019', '10:47 AM', '2019-10-22', '0000-00-00', 0, 'bc '),
(5, 2, 0, '', 1, '', '', '2019-10-03', '6260490558', '00:00:00', 'single', '', 'naveen.rajput@gmail.com', '22/10/2019', '00:00:00', '2019-10-22', '0000-00-00', 0, 'hi tttt hair vkfrj xsdjbgy'),
(6, 2, 0, '', 1, '', '', '2019-10-03', '6260490558', '00:00:00', 'single', '', 'naveen.rajput@gmail.com', '22/10/2019', '10:47 AM', '2019-10-22', '0000-00-00', 0, 'hi tttt hair vkfrj xsdjbgy'),
(7, 2, 0, '', 1, '', '', '{', '{', '{', 'single', '', 'cnfn', '22/10/2019', '10:47 AM', '2019-10-22', '0000-00-00', 0, 'vxn'),
(8, 2, 0, '', 1, '', '', 'vzb', '5959656', 'fjr', 'single', '', 'xheh', '22/10/2019', '10:47 AM', '2019-10-22', '0000-00-00', 0, 'gdb'),
(9, 2, 0, '', 1, '', '', 'vzb', '5959656', 'fjr', 'single', '', 'xheh', '22/10/2019', '10:47 AM', '2019-10-22', '0000-00-00', 0, 'gdb'),
(10, 2, 0, '', 1, '', '', 'ctctct', '6868080', 'chf  g', 'single', '', 'chvh ', '22/10/2019', '10:47 AM', '2019-10-22', '0000-00-00', 0, ' n h '),
(11, 2, 0, '', 1, '', '', 'ctctct', '6868080', 'chf  g', 'single', '', 'chvh ', '22/10/2019', '10:47 AM', '2019-10-22', '0000-00-00', 0, ' n h '),
(12, 16, 0, '', 0, '', 'G', '', '', '', '', '', '', '', '', '2019-11-01', '0000-00-00', 0, ''),
(13, 16, 0, '', 0, '', 'G', '', '', '', '', '', '', '', '', '2019-11-01', '0000-00-00', 0, ''),
(14, 16, 0, '', 0, '', 'M', '', '', '', '', '', '', '', '', '0000-00-00', '0000-00-00', 0, ''),
(15, 16, 0, '', 0, '', 'G', '', '', '', '', '', '', '', '', '2019-11-01', '0000-00-00', 0, ''),
(16, 16, 0, '', 0, '', 'G', '', '', '', '', '', '', '', '', '2019-11-01', '0000-00-00', 0, ''),
(17, 16, 0, '', 0, '', 'M', '', '', '', '', '', '', '', '', '0000-00-00', '0000-00-00', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `card_num` bigint(20) NOT NULL,
  `card_cvc` int(5) NOT NULL,
  `card_exp_month` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `card_exp_year` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `item_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_number` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `item_price` float(10,2) NOT NULL,
  `item_price_currency` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `paid_amount` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `paid_amount_currency` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `txn_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `payment_status` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `name`, `email`, `card_num`, `card_cvc`, `card_exp_month`, `card_exp_year`, `item_name`, `item_number`, `item_price`, `item_price_currency`, `paid_amount`, `paid_amount_currency`, `txn_id`, `payment_status`, `created`, `modified`) VALUES
(1, 'naveen', 'naveen.rajput@acesskynet.com', 4242424242424242, 123, '02', '20', 'Stripe Donation', 'PS123456', 50.00, 'usd', '50', 'usd', 'txn_1FXPSSJbyCOBPl3WOHjoxVk2', 'succeeded', '2019-10-25 09:51:49', '2019-10-25 09:51:49'),
(2, 'plasia', 'nkrajput.ec@gmail.com', 4242424242424242, 123, '11', '2020', 'Stripe Donation', 'PS123456', 50.00, 'usd', '50', 'usd', 'txn_1FXPvdJbyCOBPl3WCPFJ1UEI', 'succeeded', '2019-10-25 15:51:59', '2019-10-25 15:51:59'),
(3, 'Akshay Jaiswal', 'akshay@gmail.com', 4242424242424242, 123, '12', '2020', 'Stripe Donation', 'PS123456', 50.00, 'usd', '50', 'usd', 'txn_1FXQA8JbyCOBPl3WmWYNzJKo', 'succeeded', '2019-10-25 16:06:56', '2019-10-25 16:06:56'),
(4, 'plasia', 'admin@gmaiil.com', 4242424242424242, 123, '12', '2019', 'Stripe Donation', 'PS123456', 100.00, 'usd', '100', 'usd', 'txn_1FZufiJbyCOBPl3WMR75y1VS', 'succeeded', '2019-11-01 13:05:50', '2019-11-01 13:05:50'),
(5, 'plasia', 'admin@gmaiil.com', 4242424242424242, 123, '12', '2019', 'Stripe Donation', 'PS123456', 100.00, 'usd', '100', 'usd', 'txn_1FZuiwJbyCOBPl3WZj9dx8cO', 'succeeded', '2019-11-01 13:09:11', '2019-11-01 13:09:11'),
(6, 'naveen', 'connect.relier@gmail.com', 4242424242424242, 123, '12', '2019', 'Stripe Donation', 'PS123456', 100.00, 'usd', '100', 'usd', 'txn_1FZvOaJbyCOBPl3WIyik1jmm', 'succeeded', '2019-11-01 13:52:12', '2019-11-01 13:52:12'),
(7, '4242 4242 4242 4242	', 'admin@gmaiil.com', 4242424242424242, 123, '11', '2019', 'Stripe Donation', 'PS123456', 100.00, 'usd', '100', 'usd', 'txn_1FZxCuJbyCOBPl3WifHPF5o3', 'succeeded', '2019-11-01 15:48:17', '2019-11-01 15:48:17'),
(8, 'plasia', 'naveen.rajput@acesskynet.com', 4242424242424242, 123, '11', '2019', 'Stripe Donation', 'PS123456', 100.00, 'usd', '100', 'usd', 'txn_1Fa0Z4JbyCOBPl3WndMBUPy2', 'succeeded', '2019-11-01 19:23:22', '2019-11-01 19:23:22');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `payment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `txn_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_gross` float(10,2) NOT NULL,
  `currency_code` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `payer_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_status` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` float(10,2) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `image`, `price`, `status`) VALUES
(1, 'Iphone 7', 'i7.jpg', 700.00, 1),
(2, 'HTC', 'product_image1.jpg', 250.00, 1);

-- --------------------------------------------------------

--
-- Table structure for table `visiters`
--

CREATE TABLE `visiters` (
  `id` int(11) NOT NULL,
  `face_id` varchar(355) NOT NULL,
  `name` varchar(100) NOT NULL,
  `profile_pic` varchar(300) NOT NULL,
  `email` varchar(255) NOT NULL,
  `contactno` varchar(15) NOT NULL,
  `alternateno` varchar(15) NOT NULL,
  `purpose` text NOT NULL,
  `contactperson_id` int(20) NOT NULL,
  `contactperson` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL DEFAULT 'Hold',
  `remark` text NOT NULL,
  `branchid` int(11) NOT NULL,
  `companyid` int(11) NOT NULL,
  `createdate` date NOT NULL,
  `modifieddate` date NOT NULL,
  `is_delete` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `visiters`
--

INSERT INTO `visiters` (`id`, `face_id`, `name`, `profile_pic`, `email`, `contactno`, `alternateno`, `purpose`, `contactperson_id`, `contactperson`, `status`, `remark`, `branchid`, `companyid`, `createdate`, `modifieddate`, `is_delete`) VALUES
(1, '123456', 'Akshay Agarwal', 'https://images.unsplash.com/photo-1518806118471-f28b20a1d79d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=700&q=80', 'naveen.rajput@acesskynet.com', '8438240418', '8438240418', 'meet', 4, 'Vince james', 'Hold', '', 0, 7, '2019-10-23', '0000-00-00', 0),
(3, '123456', 'Akshay Agarwal', 'https://s3.amazonaws.com/facesforvarify/1571739959540jpg', 'jaiswalakshay374@gmail.com', '8438240418', '8438240418', 'hnmm', 13, 'Fifth player', 'Hold', '', 26, 29, '2019-10-23', '0000-00-00', 0),
(4, '123456', 'Akshay Agarwal', 'https://s3.amazonaws.com/facesforvarify/1571739959540jpg', 'jaiswalakshay374@gmail.com', '8438240418', '8438240418', 'hnmm', 13, 'Fifth player', 'Hold', '', 26, 29, '2019-10-23', '0000-00-00', 0),
(5, '123456', 'Akshay Agarwal', 'https://s3.amazonaws.com/facesforvarify/1571739959540jpg', 'jaiswalakshay374@gmail.com', '8438240418', '8438240418', 'hnmm', 13, 'Fifth player', 'Hold', '', 26, 29, '2019-10-23', '0000-00-00', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `branches`
--
ALTER TABLE `branches`
  ADD PRIMARY KEY (`branch_id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`emp_id`);

--
-- Indexes for table `invites`
--
ALTER TABLE `invites`
  ADD PRIMARY KEY (`invite_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`payment_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visiters`
--
ALTER TABLE `visiters`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `branches`
--
ALTER TABLE `branches`
  MODIFY `branch_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `emp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `invites`
--
ALTER TABLE `invites`
  MODIFY `invite_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `payment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `visiters`
--
ALTER TABLE `visiters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
