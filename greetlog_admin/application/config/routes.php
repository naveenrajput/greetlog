<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['default_controller']   = 'visiterlog/Registration';
$route['404_override']         = '';
$route['(:num)']               = "page/index/$1";
$route['translate_uri_dashes'] = FALSE;
//Visiterlog Controller Routes
//$route['dashboard']            = 'visiterlog/Visiterlog/Dashboard';

$route['login']                = 'visiterlog/Visiterlog/Login';
$route['chklogin']             = 'visiterlog/Visiterlog/Check_Login';

$route['logout']               = 'visiterlog/Visiterlog/Logout';
$route['register']             = 'visiterlog/Visiterlog/Registration';
$route['forgot_password']      = 'visiterlog/Visiterlog/Forgot_Password';

$route['visitorlog']              = 'visiterlog/Visiterlog/Visitor_List';
$route['checkin']              = 'visiterlog/Visiterlog/Add_Visitor';
$route['insert-visitor']       = 'visiterlog/Visiterlog/Insert_Visitor';

$route['invites']              = 'visiterlog/Visiterlog/Invite_visitor';
$route['group-invites']        = 'visiterlog/Visiterlog/Group_Invite';
$route['insert-invites']       = 'visiterlog/Visiterlog/Insert_VisitorInvites';
$route['insert-grpinvites']    = 'visiterlog/Visiterlog/Insert_groupInvites';
$route['multiples']   		   = 'visiterlog/Visiterlog/Insert_multiInvites';

$route['setup']            	   = 'visiterlog/Visiterlog/Setup_List';
$route['add-user']            	   = 'visiterlog/Visiterlog/Add_User';
$route['user']            	   = 'visiterlog/Visiterlog/User_list';
$route['insert-user']          = 'visiterlog/Visiterlog/Insert_User';

$route['import']               = 'visiterlog/Visiterlog/Import_emplist';
$route['export']               = 'visiterlog/Visiterlog/Export_emplist';
$route['vexport']              = 'visiterlog/Visiterlog/Export_visitorlist';
$route['employee']             = 'visiterlog/Visiterlog/Employee_List';
$route['add-employee']         = 'visiterlog/Visiterlog/Add_Employee';
$route['insert-emp']           = 'visiterlog/Visiterlog/Insert_Employee';
$route['edit-employee/(:any)'] = 'visiterlog/Visiterlog/Edit_Employee/$1';
$route['update-emp']           = 'visiterlog/Visiterlog/Update_Employee';

$route['branches']             = 'visiterlog/Visiterlog/Branch_List';
$route['add-branch']           = 'visiterlog/Visiterlog/Add_Branch';
$route['insert-branch']        = 'visiterlog/Visiterlog/Insert_Branch';
$route['edit-branch/(:any)']   = 'visiterlog/Visiterlog/Edit_Branchlist/$1';
$route['update-branch']        = 'visiterlog/Visiterlog/Update_Branchlist';

$route['Generate_password/(:any)']= 'visiterlog/Visiterlog/Generate_BranchPass/$1';
$route['Generate_pass']      = 'visiterlog/Visiterlog/Update_BranchPassword';
//Open Divya
$route['get_Employee']             = 'visiterlog/Visiterlog/get_Employee';
$route['verify_email']= 'visiterlog/Visiterlog/verify_emailpage';
$route['verification_email']= 'visiterlog/Visiterlog/verification_email';
//$route['create_password']    = 'visiterlog/Visiterlog/Forgot_Password';
$route['create_password/(:any)']= 'visiterlog/Visiterlog/Forgot_Password/$1';
$route['registration']         = 'visiterlog/Visiterlog/Company_registration';
$route['update_password']      = 'visiterlog/Visiterlog/Update_Password';
$route['add_company']          = 'visiterlog/Visiterlog/Add_companydetails';
$route['insertCompany']        = 'visiterlog/Visiterlog/insertCompany';
$route['companyList']          = 'visiterlog/Visiterlog/companyList';
$route['showComapny_details/(:any)']= 'visiterlog/Visiterlog/viewComapny_details/$1';

$route['updatecompany']        = 'visiterlog/Visiterlog/Update_data';


//Api Routing 

$route['user/id/(:num)/format/(:any)'] = "api/user/$1/format/$2";
$route['user/id/(:num)']       = "api/user/$1";
$route['signup']               = 'User/User/signup';
$route['signin']               = 'User/User/login';
$route['visitor']              = 'User/User/visitor';
$route['vst']                  = 'User/User/vst';
$route['invite']               = 'User/User/invite';
$route['get_emp']              = 'User/User/get_emp';
$route['get_vst']              = 'User/User/get_vst';
$route['emp_login']            = 'User/User/emp_login';
$route['emp_vst']              = 'User/User/emp_vst';
$route['requst_status']        = 'User/User/requst_status';
$route['resend']               = 'User/User/resend';
$route['checkplane']           = 'User/User/checkplane';
$route['trial_plane']          = 'User/User/trial_plane';

//Stripe payment getway Routing

$route['Stripe']                 = 'visiterlog/Visiterlog';
$route['py_action']              = 'visiterlog/Visiterlog/check';

//paypal payment getway Routing 
$route['product']              = 'visiterlog/Products';
$route['buy/(:any)']           = 'visiterlog/Products/buy/$1';
$route['success']              = 'visiterlog/paypal/success';
$route['cancel']               = 'visiterlog/paypal/cancel';
$route['ipn']                  = 'visiterlog/paypal/ipn';


// Dashboard Routing

$route['dashboard']        = 'visiterlog/Dash_controller/Dashboard';

