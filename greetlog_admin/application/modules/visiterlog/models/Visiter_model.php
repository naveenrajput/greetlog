<?php  
   class Visiter_model extends CI_Model  
   {  
      function __construct()  
      {   
         parent::__construct();  
      }
      public function get_branchIdByEmail($email)
      {
         $this->db->select('branch_id');
         $this->db->where('email_id', $email);
         return $q = $this->db->get('branches')->row();
         
      }
      /*public function BranchList_model($id)
      {
        return $query=$this->db->where('branch_id',$id)->get('branches');
      }*/
      public function Update_branchpass($pass,$id)
      {
         $this->db->where('branch_id', $id);
         $this->db->update('branches',$pass);
      }
      public function selectBranchdetails($id)
      {
         $this->db->select('*');
         $this->db->where('branch_id', $id);
         $q = $this->db->get('branches');
         return $q->result();
      } 
      //Query To check Admin Login 
      public function checklogin($email,$pass)
      {
         $this->db->select('*');
         $this->db->from('companies');
         $this->db->where('email',$email);
         $this->db->where('password',$pass);
         return $query=$this->db->get()->row();
        // print_r($this->db->last_query($query)); 
      }
      
      //Query To check user Login 
       public function Uchecklogin($email,$pass)
      {
         $this->db->select('*');
         $this->db->from('employees');
         $this->db->where('email_id',$email);
         $this->db->where('password',$pass);
         //$this->db->where('access!=""');
         return $query2=$this->db->get()->row();
      }   
     
      //Query To check Branch Login 
      public function Bchecklogin($email,$pass)
      {
         $this->db->select('*');
         $this->db->from('branches');
         $this->db->where('email_id',$email);
         $this->db->where('bpassword',$pass);
         return $query1=$this->db->get()->row();
      }  
      //Query to show Empoyee data of All employees
      public function Admin_Employeelist($companyid)
      {
         $this->db->select('*');
         $this->db->from('employees');
         $this->db->where('companyid',$companyid);
         //$this->db->where('access',"");
         return $query=$this->db->get()->result();
      }
      public function Admin_userlist($companyid)
      {
         $this->db->select('*');
         $this->db->from('employees');
         $this->db->where('companyid',$companyid);
         $this->db->where('access!=""');
         return $query=$this->db->get()->result();
      }
      public function Admin_Eployeelist($companyid)
      {
         $this->db->select('*');
         $this->db->from('employees');
         $this->db->where('companyid',$companyid);
         $this->db->where('access !=""');
         return $query=$this->db->count_all_results();
      }
      //Query to show Empoyee data of Branch wise employees
      public function Branch_Employeelist($companyid,$branchid)
      {
         $this->db->select('*');
         $this->db->from('employees');
         $this->db->where('companyid',$companyid);
         $this->db->where('branchid',$branchid);
         //$this->db->where('access',"");
         return $query=$this->db->get()->result();
      } 
      public function importemplist($data)
      {
         $this->db->insert('employees',$data);
      }
      public function exportemplist($data)
      {
         $this->db->insert('employees',$data);
      }
      //Query to Insert Empoyee data in database
      public function AddEmployee($data)
      {
         $this->db->insert('employees',$data);
      }
      //Query to Show Edit Empoyee data in database
      public function Edit_Employeelist($id)
      {
         $this->db->select('*');
         $this->db->from('employees');
         $this->db->where('emp_id',$id);
         return $query=$this->db->get();
      }
      //Query to Get company name
      public function get_companyname($companyid)
      {
         $this->db->where('id',$companyid);
         return $query = $this->db->get('companies')->result();
      }
      //Query to Get company name
      public function get_company($companyid)
      {
         $this->db->where('id',$companyid);
         return $query = $this->db->get('companies')->result();
      }
      //Query to Get branhes name
      public function get_branches($companyid)
      {
         $this->db->where('companyid',$companyid);
         return $query = $this->db->get('branches')->result();
      }
      //Query to Get invites list by company id
      public function Invites_list($companyid)
      {
         $this->db->where('companyid',$companyid);
         return $query = $this->db->get('invites')->result();
      }
      //Query to Get invites list by company id and branch id
      public function Invites_visiterlist($companyid,$branchid)
      {
         $this->db->where('companyid',$companyid);
         $this->db->where('branchid',$branchid);
         return $query = $this->db->get('invites')->result();
      }
      //Query to Insert Visitor Invites data in database
      public function Add_Invites($data)
      {
         $this->db->insert('invites',$data);
      }  
      //Query to Update Empoyee data in database
      public function Update_Employee($id,$data)
      {
         $this->db->where('emp_id',$id)->update('employees', $data);
      }
      public function Emp_id($id,$email)
      {
         $this->db->select('email_id');
         $this->db->from('employees');
         $this->db->where('emp_id',$id);
         $this->db->where('email_id !=',$email);
         return $query=$this->db->get()->row();
      }
      //Query to Show Branches data
      public function Branch_list($companyid)
        {
        $this->db->select('*');
        $this->db->from('branches');
        $this->db->where('companyid',$companyid);
        return $query=$this->db->get()->result();
        }
      public function check_email($email)
      {
         $this->db->select('email_id');
         $this->db->from('branches');
         $this->db->where('email_id',$email);
         return $query=$this->db->get()->result();
      }   
      //Query to Insert Branches data in database
      public function AddBranch($data)
      {
         $this->db->insert('branches',$data);
         return $this->db->insert_id();
      }  
      //Query to Show Edit Branch data from database
      public function Edit_Branch($id)
      {
         $this->db->select('*');
         $this->db->from('branches');
         $this->db->where('branch_id',$id);
         return $query=$this->db->get()->result();
      }
      //Query to update Branch data in database
      public function Update_Branch($id,$data)
      {
         $this->db->where('branch_id',$id)->update('branches', $data);
      } 
      //Query to Show Visitor List for Admin
      public function Admin_Visitorlist($companyid)
      {
         $this->db->select('*');
         $this->db->from('visiters');
         $this->db->where('companyid',$companyid);
         return $query=$this->db->get()->result();
      }
      //Query to Show Visitor List for sub Admin
      public function Branch_Visitorlist($companyid,$branchid)
      {
         $this->db->select('*');
         $this->db->from('visiters');
         $this->db->where('companyid',$companyid);
         $this->db->where('branchid',$branchid);
         return $query=$this->db->get()->result();
      }
      public function InsertVisiterdetails($data)
      {
         $this->db->insert('visiters',$data);
      }     
      //open Divya


      //company registration
      public function C_registration($data)
      {
         $this->db->insert('companies',$data);
         return $this->db->insert_id();
      }

      //company registration
      public function get_companyIdByEmail($email)
      {
         $this->db->select('id');
         $this->db->where('email', $email);
         return $q = $this->db->get('companies')->row();
         
      }
      //Query To select plan expiry date  
      public function checksession($id)
      {
         $this->db->select('expirydate');
         $this->db->from('companies');
         $this->db->where('id',$id);
         return $query=$this->db->get()->row();
      }

       //Query to Insert is_expired data in database
      public function insert_expired($data,$id)
      {
         $this->db->where('id', $id);
         $this->db->update('companies',$data);
      }

       //Query to Insert is_expired data in database
      public function Update_passModel($data,$id)
      {
         $this->db->where('id', $id);
         $this->db->update('companies',$data);
      }

      //Insert Company Details
      public function insertCompanydetails($data,$id){

         $insert = $this->db->where('id', $id)->update('companies',$data);
      }

      //get Company table
      public function companyList_model($id)
      {
        return $query=$this->db->where('id',$id)->get('companies');
      }

      //select Company Details
      public function selectCompanydetails($id)
      {
         $this->db->select('*');
         $this->db->where('id', $id);
         $q = $this->db->get('companies');
         return $q->result();
      }  

      //Edit or update Company Details
      public function editCompanydetails($data, $id)
      {
         $this->db->where('id', $id);
         $query =  $this->db->update('companies', $data);
         //print_r($this->db->last_query($query)); 
      }
      
      public function get_BranchEmail($email)
      {
         $this->db->select('branch_id');
         $this->db->where('email_id', $email);
         return $q = $this->db->get('branches')->row();
         
      }
      public function get_BranchContact($contactno)
      {
         $this->db->select('branch_id');
         $this->db->where('contactno', $contactno);
         return $q = $this->db->get('branches')->row();
         
      }
      
      public function get_EmployeeEmail($email)
      {
         $this->db->select('emp_id');
         $this->db->where('email_id', $email);
         return $q = $this->db->get('employees')->row();
         
      }
      //count total emplpyee
      public function total_Employee(){
         return $this->db->count_all('employees');
      }
      
      //count total emplpyee
      public function total_CEmployee($companyid){
         $this->db->from('employees');
         $this->db->where('companyid', $companyid);
         return $this->db->count_all_results();
      }
        //count total emplpyee
      public function total_BEmployee($companyid,$branchid){
         $this->db->from('employees');
         $this->db->where('companyid', $companyid);
         $this->db->where('branchid', $branchid);
         return $this->db->count_all_results();
      }
      //Close Divya
   }