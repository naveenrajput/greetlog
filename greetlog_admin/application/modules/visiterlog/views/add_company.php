<style>
 
#map {
  width:100%;
  height:400px;

}

@media (max-width: 767px) {
  #map {
  width:100%;
  height:200px;

}  
}
</style>

<!-- Begin Page Content -->
<div class="container-fluid">
   
   <!-- Page Heading -->
   <h1 class="h3 mb-2 text-gray-800">Company Details</h1>
   <?php
   
   if(isset($breadcrumb)&&  !is_null($breadcrumb)){
   ?> 
   <div class="span10" style="margin-left:5px;">
      
         <ul class="breadcrumb">
            <?php
               echo $breadcrumb ;             
            ?>     
         </ul>
      
   </div>
   <?php 
      }
      ?>
   <div class="card shadow mb-4">
      
      <div class="card-body">
         <div class="form1 p-t-20">
            <form action="<?php echo base_url('insertCompany');?>" id="myForm" method="post" enctype="multipart/form-data" id="bootstrapForm" novalidate="">
               <?php 
               if(isset($c_detail)&&  !is_null($c_detail)){
                  foreach ($c_detail as $data){
                    
                  ?>
               <input type="hidden" name="u_id" value="<?php echo $data->id; ?>">
               <div class="form-row">
                  <div class="col-md-6">
                     <div class="form-row">
                        <!-- <div class="form-group col-md-6">
                           <select name="companytype" class="form-control"  >
                              <option value="<?php //echo $data->companytype; ?>"><?php //echo $data->companytype; ?></option>
                              <option value="Company">Company</option>
                              <option value="Corporation">Corporation</option>
                              <option value="School/Institute">School/Institute</option>
                              <option value="College/University">College/University</option>
                           </select>
                        </div> -->
                        <div class="form-group col-md-12">
                           <label class="has-float-label">
                              <input type="text" class="form-control" name="companyname" placeholder="-" value="<?php echo $data->companyname; ?>" disabled>
                              <span class="" for="organizationname">Organization Name</span>
                           </label>
                        </div>
                        <div class="form-group col-md-12">
                           <label class="has-float-label">
                              <input type="text" class="form-control" name="ownername" placeholder="-" value="<?php echo $data->ownername; ?>" required>
                              <span class="" for="ownername">Owner Name</span>
                              <div class="valid-feedback"></div>
                              <div class="invalid-feedback">Please enter owner name. This field is required.</div>
                           </label>
                        </div>
                        <div class="form-group col-md-12">
                           <label class="has-float-label">
                              <input type="text" class="form-control" name="slogan" placeholder="-" value="<?php if(!empty($data->slogan)){ echo $data->slogan; }else{ echo set_value('slogan'); }  ?>" >
                              <span class="" for="companytag">Company Tag</span>
                           </label>
                        </div>
                        
                        <div class="form-group col-md-12">
                           <label class="has-float-label">
                              <input type="email" class="form-control" name="email" placeholder="-" value="<?php echo $data->email; ?>" disabled >
                              <span class="" for="email">Eamil ID</span>
                           </label>
                        </div>
                        <div class="form-group col-md-12 contact_fld">
                           <label class="form-group has-float-label">
                              <select  id="countries_phone1" class="form-control bfh-countries" data-country="IN"></select>
                              <span>Country</span>
                              <div class="valid-feedback"> </div>
                              <div class="invalid-feedback">Please select option. This field is required.</div>
                            </label>
                        </div>
                        
                        <div class="form-group col-md-12">
                           <label class="has-float-label">
                              <input type="text" class="form-control bfh-phone" data-country="countries_phone1" name="contactNo" placeholder="-" value="<?php echo $data->contactno; ?>" required>
                              <span class="" for="contactno">Contact No.</span>
                              <div class="valid-feedback"></div>
                              <div class="invalid-feedback">Please enter contact no. This field is required.</div>
                           </label>
                        </div>
                       <!--  <div class="form-group col-md-6">
                           <span class="has-float-label">
                              <input type="text" class="form-control" name="alternateNo" placeholder="-" value="<?php //echo $data->alternateno; ?>" >
                              <label class="" for="alternateNo">Alternate Number(Optional)</label>
                           </span>
                        </div> -->
                         <div class="form-group col-md-12">
                        <label class="has-float-label">
                        <input class="form-control" id="address" name="address"  placeholder="-" value="<?php if(!empty($data->address)){ echo $data->address; }else{ echo set_value('address'); }  ?>"  required>
                         <span class="" >Address</span>
                           <div class="valid-feedback"></div>
                           <div class="invalid-feedback">Please enter a valid email. This field is required.</div>
                        </label>
                    <!--<input id="submit" type="button" class="btn btn-outline-secondary" value="Get Location">--->
                     </div>
                     </div>
                  </div>
                  <div class="col-md-6">
                       <div class="col-md-12">
                         
                           <div id="map" ></div>
                      
                     </div>
                   
                  </div>
               </div>
                 
               <div class="m-b-20">
              <div class="card ">
               
                <div class="card-body">
                    
                  <div class=" mb-1"><strong>Logo</strong></div>
                    <p class="small ">Choose a image to appear on your welcome screen in place of your logo.</p>
                 <!--pic start-->
                 <div class="form-row ">
                 <div class="col-md-3">
                     <div class="form-group ">
                        <div class="avatar-upload">
                           <div class="avatar-edit">
                              <input type='file' id="imageUpload" name="logo">
                              <label for="imageUpload"></label>
                           </div>
                           <div class="avatar-preview">
                              <div id="imagePreview" style="background-image: url(<?php if (!empty($data->logo)) {
                                 echo $data->logo;
                                 }else{ ?><?php echo base_url('uploads/logo.jpg'); ?><?php } ?>  );">
                              </div>
                           </div>
                        </div>
                        <span id="err_img"></span>
                     </div>
                     </div>
                     <div class="col-md-9 m-t-30 m-b-20">
                         <p>We recommend uploading a transparent PNG cropped to the edges of your logo and up to 600px width or 600px heigh.</p>

                          <strong>  Tip:</strong> To further customize the look of your device, add a welcome image.
                         </div>
                    
                     <!--pic close-->
                
                </div>
                </div>
              </div>

            </div>
           
               <div class="form-row m-t-10">
                  <div class="form-group col-md-4">
                     <label class="has-float-label">
                        <select name="plan" class="form-control" value="<?php echo set_value('plan'); ?>" >
                           <option value="15 days">15 Days Trial</option>
                           <option value="3 months">3 Months</option>
                           <option value="6 months">6 Months</option>
                           <option value="12 months">12 Months</option>
                        </select>
                        <span class="" for="sendType">Plan</span>
                        <div class="valid-feedback"></div>
                        <div class="invalid-feedback">Please select plan. This field is required.</div>
                     </label>
                  </div>
               </div>
               <div class="form-row clearfix">
                  <div class="form-group col-md-12">
                     <!-- <span class="has-float-label"> -->
                        <label for="sendType">Send Type : </label>
                        <div class="custom-control custom-radio custom-control-inline">
                           <input type="radio" id="customRadioInline1" name="sendtype" class="custom-control-input" value="Email" <?php if($data->sendtype =="Email"){echo "checked";}else{ echo "checked";}?> >
                           <label class="custom-control-label" for="customRadioInline1">By Email</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                           <input type="radio" id="customRadioInline2" name="sendtype" class="custom-control-input" value="Message" <?php if($data->sendtype =="Message"){echo "checked";}?> >
                           <label class="custom-control-label" for="customRadioInline2">By Message</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                           <input type="radio" id="customRadioInline1" name="sendtype" class="custom-control-input" value="whatsapp" <?php if($data->sendtype =="whatsapp"){echo "checked";}?> >
                           <label class="custom-control-label" for="customRadioInline1">By Whats App</label>
                        </div>
                     <!-- </span> -->
                  </div>
               </div>
             
         
                
               <!-- <br>* PLAN :  ₹ 199.00/mo when you renew<br> --> 
               <button type="submit" id="smt" class="btn btn-green shadow-sm" >submit</button>
               <?php }  }?>
            </form>
         </div>
      </div>
   </div>
</div>
<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5k0EYWAfDBuiUsu4oSZKgew__dw0oENg&libraries=places&callback=initMap"
         async defer></script>
<script type="text/javascript">
  
/*$("#myForm").submit(function(event) {
         
             // make selected form variable
             var vForm = $(this);
             
           
             if (vForm[0].checkValidity() === false) {
               event.preventDefault()
               event.stopPropagation()
             } else {
              
               // Replace alert with ajax submit here...
               //alert("your form is valid and ready to send");
               
             }
             
             // Add bootstrap 4 was-validated classes to trigger validation messages
             vForm.addClass('was-validated');
             
          
         });*/

   
   // profile-pic
   function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function(e) {
              $('#imagePreview').css('background-image', 'url('+e.target.result +')');
              $('#imagePreview').hide();
              $('#imagePreview').fadeIn(650);
          }
          reader.readAsDataURL(input.files[0]);
      }
   }
   $("#imageUpload").change(function() {
      readURL(this);
   });
   
   $("#myForm").submit(function(e) {
    var inp = document.getElementById('imageUpload');
   
   
    if (inp.files.length == 0) {
        $('#err_img').css('color','red');
        $("#err_img").html("Please uplolad logo");
        // setTimeout(function() {
        //   $("#err_img").hide();
        // }, 1000);
        
        // $('#err_img').css('color','red');
        // $("#err_img").html("Please uplolad logo");
        e.preventDefault();
    }
      
   });
   
   /*map*/
  function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 8,
    center: {lat: 28.6139, lng: 77.2090}
  });
  var geocoder = new google.maps.Geocoder();

  document.getElementById('address').addEventListener('change', function() {
    geocodeAddress(geocoder, map);
  });
}

function geocodeAddress(geocoder, resultsMap) {
  var address = document.getElementById('address').value;
  geocoder.geocode({'address': address}, function(results, status) {
    if (status === 'OK') {
      resultsMap.setCenter(results[0].geometry.location);
      var marker = new google.maps.Marker({
        map: resultsMap,
        position: results[0].geometry.location
      });
    } else {
      alert('Geocode was not successful for the following reason: ' + status);
    }
  });
}
   
   
</script>