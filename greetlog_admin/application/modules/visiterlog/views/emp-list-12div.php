 
      <div class="container-fluid">
          <!-- Page Heading -->
          <div class="row">
            <div class="col-md-8">
              <h1 class="h3 mb-2 text-gray-800">Employees List</h1>
            </div>
            <div class="col-md-4">
              <a href="<?php echo base_url('add-employee'); ?>" class="btn btn-green">Add Employee</a>
              <a href="javascript:void(0);" class="btn btn-green" data-toggle="modal" data-target="#Mymodal"><i class="plus"></i>Import</a>
              <a href="<?php echo base_url('export'); ?>" class="btn btn-green"><i class="exp"></i> Export</a>
            </div>
             <!-- CSV file upload form -->
           <!--  <div class="col-md-12" id="importFrm" style="display: none;">
                <form action="<?php //echo base_url('import'); ?>" method="post" enctype="multipart/form-data">
                    <input type="file" name="file"/>
                    <input type="submit" class="btn btn-green" name="importSubmit" value="IMPORT">
                </form>
            </div> -->
          </div>
            
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>S.No</th>
                      <th>Profile</th>
                      <th>Name</th>
                      <th>Contact</th>
                      <!-- <th>Alternate Contact</th> -->
                      <th>Email</th>
                      <th>Designation</th>
                      <th>Create Date</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>S.No</th>
                      <th>Profile</th>
                      <th>Name</th>
                      <th>Contact</th>
                     <!--  <th>Alternate Contact</th> -->
                      <th>Email</th>
                      <th>Designation</th>
                      <th>Create Date</th>
                      <th>Action</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    <?php if (isset($data)) {
                         $i=1; foreach ($data as $emp) {
                    ?>
                    <tr>
                      <td><?php echo $i; ?></td>
                      <td><img src="<?php echo $emp->profile_pic; ?>" style="width:50px;"></td>
                      
                      <td><?php echo $emp->empname; ?></td>
                      <td><?php echo $emp->contact_num; ?></td>
                      <!-- <td><?php //echo $emp->alternate_num; ?></td> -->
                      <td><?php echo $emp->email_id; ?></td>
                      <td><?php echo $emp->designation; ?></td>
                      <td><?php echo $emp->createdate; ?></td>
                      <td><a href="<?php echo base_url('edit-employee/').$emp->emp_id; ?>">Edit</a></td>
                    </tr>
                  <?php $i++; } } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>

        <!-- /.container-fluid -->

      </div>
<!-- End of Main Content -->
<!-- ALL MODALS VIEW -->
<div class="modal fade" id="Mymodal" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content" id="Mymodal_view">
      <div class="modal-header">
       <h4>Import CSV File</h4>
       <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
      </div>
      <div class="modal-body">
        <form action="<?php echo base_url('import'); ?>" method="post" enctype="multipart/form-data">
            <input type="file" name="file"/>
            <input type="submit" class="btn btn-green" name="importSubmit" value="IMPORT">
        </form>
      </div>
    </div>
    <!-- /.modal-content --> 
  </div>
<!-- /.modal-dialog --> 
</div>

<!-- Show/hide CSV upload form -->
<!-- <script>
function formToggle(ID){
    var element = document.getElementById(ID);
    if(element.style.display === "none"){
        element.style.display = "block";
    }else{
        element.style.display = "none";
    }
}
</script> -->