
        <!-- Begin Page Content -->
       <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Form</h1>
          <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="#">official DataTables documentation</a>.</p>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Form Example</h6>
            </div>
            <div class="card-body">
              <div class="form">
						<form>
  <div class="form-row">
  <div class="form-group col-md-6">
     
      <input type="text" class="form-control" id="inputName" placeholder="Name" >
    </div>
  <div class="form-group col-md-6">
     
      <input type="text" class="form-control" id="inputName" placeholder="Mobile" >
    </div>
    <div class="form-group col-md-6">
      <!---<label for="inputEmail4">Email</label>-->
      <input type="email" class="form-control" id="inputEmail4" placeholder="Email">
    </div>
   <div class="form-group col-md-6">
      <!---<label for="inputEmail4">Email</label>-->
      <input type="email" class="form-control" id="inputEmail4" placeholder="Email">
    </div>
	<div class="form-group col-md-6">
      <!---<label for="inputEmail4">Email</label>-->
      <input type="email" class="form-control" id="inputEmail4" placeholder="Email">
    </div>
  </div>
  <div class="form-group ">
  
    <input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St">
  </div>
  <div class="form-group">
   
    <input type="text" class="form-control" id="inputAddress2" placeholder="Apartment, studio, or floor">
  </div>
  <div class="form-group">
  <div class="custom-file">
  <input type="file" class="custom-file-input" id="customFile">
  <label class="custom-file-label" for="customFile">Choose file</label>
</div>
</div>
  <div class="form-row">
    <div class="form-group col-md-6">
     
      <input type="text" class="form-control" id="inputCity">
    </div>
    <div class="form-group col-md-4">
   
      <select id="inputState" class="form-control">
        <option selected>Choose...</option>
        <option>...</option>
      </select>
    </div>
    <div class="form-group col-md-2">
     
      <input type="text" class="form-control" id="inputZip">
    </div>
  </div>
  <div class="form-group">
    <div class="form-check">
      <input class="form-check-input" type="checkbox" id="gridCheck">
      <label class="form-check-label" for="gridCheck">
        Check me out
      </label>
    </div>
  </div>
  
  <a href="#" class=" btn btn-green shadow-sm"> Submit</a>
  
</form>
			  </div>
            </div>
          </div>

        </div>

        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->