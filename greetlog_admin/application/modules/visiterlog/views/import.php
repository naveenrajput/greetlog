<?php

if(isset($_POST['importSubmit'])){
    
    // Allowed mime types
    $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
    
    // Validate whether selected file is a CSV file
    if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'], $csvMimes)){
        
        // If the file is uploaded
        if(is_uploaded_file($_FILES['file']['tmp_name'])){
            
            // Open uploaded CSV file with read-only mode
            $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
            
            // Skip the first line
            fgetcsv($csvFile);
            $cart = array();
            // Parse data from CSV file line by line
            while(($line = fgetcsv($csvFile)) !== FALSE){
                // Get row data
                $sno   = $line[0];
                $name   = $line[1];
                $email  = $line[2];
                $phone  = $line[3];
                $deg    = $line[4];
                $status = $line[5];
                
                // Check whether member already exists in the database with the same email
                $prevQuery = "SELECT emp_id FROM employees WHERE email_id = '".$email."' ";
                $prevResult=$this->db->query($prevQuery);
                if($prevResult->result_id->num_rows > 0){
                        
                   
                array_push($cart,$sno.'-'.$email);  
            
                    //echo $email;die;
                    // Update member data in the database
                    /*$this->db->query("UPDATE employees SET empname = '".$name."', contact_num = '".$phone."', designation = '".$deg."', modifieddate = NOW() WHERE email_id = '".$email."' ");*/
                }else{
                    // Insert member data in the database
                    $this->db->query("INSERT INTO employees (empname, email_id, contact_num,designation,companyid,createdate) VALUES ('".$name."', '".$email."', '".$phone."','".$deg."','".$cid."', NOW())");
                }
            }
            
            //print_r($cart);die;
            // Close opened CSV file
            fclose($csvFile);
            
            //$qstring = '?status=succ';
        }/*else{
            $qstring = '?status=err';
        }
    }else{
        $qstring = '?status=invalid_file';
    }*/
}
}
//$this->load->view('emp-list',compact('cart'));
// Redirect to the listing page
//header("Location: emp-list.php".$qstring);
?>


<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <div class="alert alert-danger alert-dismissible fade in">
    <a href="<?php echo base_url('employee') ?>" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>
        <?php if (isset($cart)) {
                foreach ($cart as $value){
                   echo 'Error occurs on Line NO-'.$value.' Already Exists'.'<br>';
                }
            }
        ?>        
    </strong>
  </div>
</div>

</body>
</html>
